const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoPrefix = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    cleaner = require('gulp-clean'),
    concat = require('gulp-concat'),
    rename = require("gulp-rename"),
    minify = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    pipeline = require('readable-stream').pipeline,
    imgMin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();
const path = {
    dist:{
        html:'dist',
        css:'dist/css',
        js:'dist/js',
        img : 'dist/img',
        ico: 'dist/favicon',
        self:'dist'
    },
    src : {
        html:'src/*.html',
        reset: 'src/scss/_reset.scss',
        scss : 'src/scss/*.scss',
        js : 'src/js/*.js',
        img: 'src/img/**/**/*.*',
        ico: 'src/favicon/*.*',
    }
};

/**************** F U N C T I O N S ***************/
const htmlBuild = () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
);

const styleVendor = () => (
    gulp.src([
        './node_modules/@fortawesome/fontawesome-free/css/all.css'
        // './node_modules/bootstrap/dist/css/**/*.css'
    ])
        .pipe(autoPrefix('last 8 versions'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);
const scssBuild = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoPrefix('last 8 versions'))
        .pipe(cleanCSS({compatibility: 'ie8'}))   // .pipe(autoPrefix(['> 0.01%', 'last 100 versions']))
        .pipe(rename({suffix: '.min'}))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const scriptVendor = () => (
    gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/@fortawesome/fontawesome-free/js/all.js'
        // './node_modules/bootstrap/dist/js/bootstrap.js'
    ])
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
);
const jsBuild = () => (
    gulp.src(path.src.js)
        // .pipe(minify())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
);

const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(imgMin())
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);

// const icoBuild = () => (
//     gulp.src(path.src.ico)
//         .pipe(gulp.dest(path.dist.ico))
//         .pipe(browserSync.stream())
// );

const cleanProd = () =>(
    gulp.src(path.dist.self, {allowEmpty: true})
        .pipe(cleaner())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(browserSync.stream())
);

/****************** W A T C H E R ***************/
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch(path.src.html, htmlBuild).on('change',browserSync.reload);
    gulp.watch(path.src.scss, scssBuild).on('change',browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change',browserSync.reload);
    gulp.watch(path.src.img, imgBuild).on('change',browserSync.reload);
};
/**************** T A S K S ****************/
gulp.task('build',gulp.series(
    cleanProd,
    scssBuild,
    imgBuild,
    jsBuild,
    // icoBuild,
    scriptVendor,
    styleVendor,
    htmlBuild
));

gulp.task('dev',gulp.series(
    watcher
));

// gulp.task('default',gulp.series(
//     cleanProd,
//     htmlBuild,
//     scssBuild,
//     imgBuild,
//     jsBuild,
//     icoBuild,
//     scriptVendor,
//     styleVendor,
//     watcher
// ));

